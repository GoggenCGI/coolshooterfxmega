﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;

public class BallScript : MonoBehaviour {

    private float speed = 100f;
    bool downwards;
    Rigidbody2D body;
    Vector3 currentPos;
	// Use this for initialization
	void Start () {
        body = this.GetComponent<Rigidbody2D>();
        downwards = true;
        this.GetComponent<Rigidbody2D>().AddForce(-transform.up * speed);
	}

    private void FixedUpdate()
    {
        currentPos = body.transform.position;
    }

    // Update is called once per frame
    void Update () {
        downwards = body.transform.position.y < currentPos.y;
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if(collision.gameObject.tag == "Wall")
        {
            Debug.Log("Hit Wall");
            if(body.transform.position.x > 0)
            {
                body.AddForce(transform.right * speed);
                if(downwards)
                    this.GetComponent<Rigidbody2D>().AddForce(transform.up * speed);
                else
                    this.GetComponent<Rigidbody2D>().AddForce(-transform.up * speed);
            }
            else
            {
                body.AddForce(-transform.right * speed);
                if (downwards)
                    this.GetComponent<Rigidbody2D>().AddForce(transform.up * speed);
                else
                    this.GetComponent<Rigidbody2D>().AddForce(-transform.up * speed);
            }
        }
        if (this.gameObject.transform.position.x > collision.gameObject.transform.position.x)
        {
            Debug.Log("Hit Right Side");
            body.AddForce(transform.right * speed);
        }
        else
        {
            Debug.Log("Hit Left Side");
            body.AddForce(-transform.right * speed);
        }
        

        if (downwards)
        {
            this.GetComponent<Rigidbody2D>().AddForce(transform.up * speed);
            downwards = false;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(-transform.up * speed);
            downwards = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Background")
        {
            Debug.Log("Game Over");
        }
    }
}
