﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {


    public Paddle playerOne, playerTwo;

    /*
     * Awake method starts befor anything else in the game.
     * This methods sets the two paddles to their respective Ids.
     * It also adds a starting tweak to the speed of 4.
     */
    private void Awake()
    {
        playerOne.paddleId = 1;
        playerTwo.paddleId = 2;

        playerOne.UpgradeSpeed(4f);
        playerTwo.UpgradeSpeed(4f);
    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
