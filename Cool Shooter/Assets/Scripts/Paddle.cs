﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

    /*
     * Added parameters for paddle.
     * Paddle ID tells the game which player it is.
     * Bounce Effect will be used later when determining bounce.
     * Size will be used to determine the size of the paddle.
     * Paddle Speed determines the speed of the paddle.
     * 
     * playerOne bool says wether or not this paddle is Player One.
     * 
     * Sprite Renderer changes the color of player two.
     */
    public int paddleId;
    private int bounceEffect = 1;
    private int size = 1;
    private float paddleSpeed = 1.0f;

    bool playerOne;

    SpriteRenderer thisSpritesRenderer;

    public void UpgradeSpeed(float speed){
        this.paddleSpeed = speed;
    }

    private void Start()
    {
        Debug.Log(this.paddleId);
        playerOne = paddleId == 1 ? true : false;
        thisSpritesRenderer = gameObject.GetComponent<SpriteRenderer>();

        if(!playerOne)
        {
            thisSpritesRenderer.color = Color.green;
        }
            
    }

    // Update is called once per frame
    void Update () {

        if (playerOne)
        {
            if (Input.GetKey("a"))
            {
                transform.Translate(-1 * paddleSpeed * Time.deltaTime, 0, 0);
            }

            if (Input.GetKey("d"))
            {
                transform.Translate(1 * paddleSpeed * Time.deltaTime, 0, 0);
            }
        }else{
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(-1 * paddleSpeed * Time.deltaTime, 0, 0);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(1 * paddleSpeed * Time.deltaTime, 0, 0);
            }
        }

		
	}
}
