﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEnum : MonoBehaviour {

    enum Effects{
        Faster,
        Smaller,
        Bigger,
        Weaker,
        Bouncier
    }
}
